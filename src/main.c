//
// Created by dimma on 02.01.2022.
//

#define _DEFAULT_SOURCE

#include <unistd.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static struct block_header* get_block(void* memory);

void print_line()
{
    size_t count = 60;
    while(count > 0)
    {
        fprintf(stdout, "==");
        count--;
    }
}

void free_heap (void *heap){
    struct block_header* header = heap;
    while(header)
    {
        _free(header->contents);
        header = header->next;
    }
    _free(((struct block_header*) heap)->contents);
}

//Normal memory allocation
void normal_memory_allocation(void* heap)
{
    fprintf(stdout, "\nTest #1\n");
    print_line();
    void* memory1 = _malloc(0);
    void* memory2 = _malloc(10);
    void* memory3 = _malloc(200);
    if(!memory1 || !memory2 || !memory3)
    {
        fprintf(stdout, "\nTest #1 failed\n");
        if(!memory1)
        {
            fprintf(stdout, "\nnode: no memory is allocated for the first block\n");
        }
        if(!memory2)
        {
            fprintf(stdout, "\nnode: no memory is allocated for the second block\n");
        }
        if(!memory1)
        {
            fprintf(stdout, "\nnode: no memory is allocated for the third block\n");
        }
    }
    else
    {
        fprintf(stdout, "\nMemory allocated\n");
    }
    debug_heap(stdout, heap);
    print_line();
}


void deleting_one_selected_blocks(void* heap)
{
    fprintf(stdout, "\nTest #2\n");
    print_line();
    _malloc(0);
    _malloc(10);
    void* markedMemory = _malloc(666);
    fprintf(stdout, "\nHeap without deleting allocated marked block\n");
    debug_heap(stdout, heap);
    struct block_header* blockHeader = get_block(markedMemory);
    _free(markedMemory);
    debug_heap(stdout, heap);
   if(blockHeader->is_free)
    {
        fprintf(stdout, "\nBlock is free\n");
        fprintf(stdout, "\nHeap with deleting allocated marked block\n");
        debug_heap(stdout, heap);
    }
    else
    {
        fprintf(stdout, "\nBlock is not free\n");
    }
    print_line();
}

void deleting_two_selected_blocks(void* heap)
{
    fprintf(stdout, "\nTest #3\n");
    print_line();
    _malloc(0);
    _malloc(10);
    void* marked_memory_1 = _malloc(666);
    void* marked_memory_2 = _malloc(228);
    fprintf(stdout, "\nHeap without deleting allocated marked block\n");
    debug_heap(stdout, heap);
    _free(marked_memory_2);
    _free(marked_memory_1);
    struct block_header* block_header_1 = get_block(marked_memory_1);
    struct block_header* block_header_2 = get_block(marked_memory_2);
    if(block_header_1->is_free || block_header_2 -> is_free)
    {
        fprintf(stdout, "\nBlocks are free\n");
        fprintf(stdout, "\nHeap with deleting allocated marked block\n");
        debug_heap(stdout, heap);
    }
    else
    {
        if(!block_header_1->is_free)
        {
            fprintf(stdout, "\nFirst block isn't free\n");
        }
        if(!block_header_1->is_free)
        {
            fprintf(stdout, "\nSecond block isn't free\n");
        }
    }
    print_line();
}

void grow_heap_test(void* heap)
{
    fprintf(stdout, "\nTest #4\n");
    print_line();
    _malloc(0);
    _malloc(900);
    _malloc(1500);
    fprintf(stdout, "\nHeap without expansion\n");
    debug_heap(stdout, heap);
    void* marked_memory_1 = _malloc(49000);
    struct block_header* block_header_1 = get_block(marked_memory_1);
    if(block_header_1->is_free)
    {
        fprintf(stdout, "No memory allocated");
    }
    debug_heap(stdout, heap);
    print_line();
}

void test_extends_failed(void* heap)
{
    fprintf(stdout, "\nTest #5\n");
    print_line();
    _malloc(0);
    _malloc(900);
    void* memory = _malloc(1500);
    struct block_header* addr = get_block(memory);
    if(addr->next)
    {
        addr = addr->next;
    }
    void* test_addr = (uint8_t*) addr + size_from_capacity(addr->capacity).bytes;
    test_addr = mmap( (uint8_t*) (getpagesize() * ((size_t) test_addr / getpagesize() +
                                                            (((size_t) test_addr % getpagesize()) > 0))), 1000,
            PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,0, 0);
    debug_heap(stdout, test_addr);
    _malloc(690000);
    debug_heap(stdout, heap);
    print_line();
}


int main() {

    void* heap = heap_init(30000);
    debug_heap(stdout, heap);
    normal_memory_allocation(heap);
    free_heap(heap);
    deleting_one_selected_blocks(heap);
    free_heap(heap);

    deleting_two_selected_blocks(heap);
    free_heap(heap);

    grow_heap_test(heap);
    free_heap(heap);

    test_extends_failed(heap);
    free_heap(heap);
    return 0;
}

static struct block_header* get_block(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}